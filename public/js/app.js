var randomElement = function(items) {
  index = Math.floor(Math.random() * items.length);
  return items[index];
}

var app = new Vue({
  el: '#app',
  data: {
    icebreakers: [],
    current_icebreaker: 'Loading...'
  },
  methods: {
    pickIcebreaker: function(){
      this.current_icebreaker = randomElement(this.icebreakers);
    }
  },
  created: function () {
    fetch("./icebreakers.json")
      .then(response => response.json())
      .then(json => {
        this.icebreakers = json;
        this.pickIcebreaker()
      });
    }
});
