require 'csv'
require 'httparty'

class CsvLoader
  def initialize(uri:)
    @uri = uri
  end

  def first_column
    @first_column ||= csv.map(&:first)
  end

  def csv
    @csv ||= CSV.parse(raw_csv!, headers: false)
  end

  private

  def raw_csv!
    @raw_csv ||= HTTParty.get(@uri, format: :plain).body.force_encoding('utf-8')
  end
end
