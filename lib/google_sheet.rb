class GoogleSheet
  def initialize(file_key:)
    @file_key = file_key
  end

  def export_uri
    "https://docs.google.com/spreadsheets/d/#{@file_key}/export?format=csv&id=#{@file_key}&gid=0"
  end

  def publish_uri
    "https://docs.google.com/spreadsheets/d/e/#{@file_key}/pub?output=csv"
  end
end
