map "/" do
  use Rack::Static, root: File.expand_path('public'), urls: [""], index: 'index.html'
  run lambda {}
end
