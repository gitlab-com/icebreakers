require 'json'
require_relative './google_sheet.rb'
require_relative './csv_loader.rb'

class SourceUpdater
  def initialize(csv_uri:, json_path:)
    @csv_uri = csv_uri
    @json_path = json_path
  end

  def call
    items = CsvLoader.new(uri: @csv_uri).first_column

    dump_items(items)
  end

  private

  def dump_items(items)
    File.open(@json_path, 'w') do |f|
      JSON.dump(items, f)
    end
  end
end
